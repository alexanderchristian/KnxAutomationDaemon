package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.logicplugin.Logic;
import de.root1.kad.logicplugin.cron.Scheduled;

public class Logic106NachtSzRolladen extends Logic {

    KnxAddress gaRolladenSZ = getAddress("Sonnenschutz Bewegen Schlafzimmer - Abs. Position");
    KnxAddress gaRolladenSperreSZ = getAddress("Sonnenschutz Sperren Schlafzimmer");

    @Override
    public void init() {
        setPA("1.0.106");
    }

    // schedule at 04:30am
    @Scheduled(cron = "30 04 * * *")
    public void rolladenRunter() {
        log.info("Rolladen SZ unlock");
        write(gaRolladenSperreSZ, "0"); // entsperren
        sleep(500);
        log.info("Rolladen SZ 100%");
        write(gaRolladenSZ, "100"); // runter fahren
        sleep(500);
        log.info("Rolladen SZ lock");
        write(gaRolladenSperreSZ, "1"); // sperren
    }

}
