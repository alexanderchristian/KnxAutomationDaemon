package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;

public class Logic109NachtLichtBadDGTimo extends Logic {

    KnxAddress gaPräsenzFlurDG;
    KnxAddress gaTagNachtModusInvertiert; // 1 = Nacht; 0=Tag
    KnxAddress gaLicht;

    private boolean praesenzFlurDG;
    private boolean tagNachtModus;

    @Override
    public void init() {
        setPA("1.0.109");

        gaPräsenzFlurDG = getAddress("Präsenz Flur DG");
        gaTagNachtModusInvertiert = getAddress("Daemmerung Tag-Nacht Modus invertiert"); // 1 = Nacht; 0=Tag
        gaLicht = getAddress("Licht Schalten Bad DG Steckdose WC-Eck");

        listenTo(gaPräsenzFlurDG);
        listenTo(gaTagNachtModusInvertiert);
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {

        if (ga.equals(gaPräsenzFlurDG)) {
            praesenzFlurDG = getValueAsBoolean(value);

        } else if (ga.equals(gaTagNachtModusInvertiert)) {

            tagNachtModus = getValueAsBoolean(value);
        }

        if (tagNachtModus && praesenzFlurDG) {
            write(gaLicht, "1");
            log.info("Timo Bad Nachtlicht AN");
        } else {
            log.info("Timo Bad Nachtlicht AUS");
            write(gaLicht, "0");
        }
    }

}
