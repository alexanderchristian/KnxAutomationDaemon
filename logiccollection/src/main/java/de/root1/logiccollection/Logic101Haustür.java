package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;

public class Logic101Haustür extends Logic {

    KnxAddress gaFingerhash;
    KnxAddress gaMessage;

    @Override
    public void init() {
        setPA("1.0.101");
        
        gaFingerhash = getAddress("Haustür Fingerhash");
        gaMessage = getAddress("Haustür Meldung");
        
        listenTo(gaFingerhash);
        listenTo(gaMessage);
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {
        if (ga.equals(gaFingerhash)){
            log.info("Fingerhash: {}", String.format("0x%02x", Integer.parseInt(value)));
        } else if (ga.equals(gaMessage)) {
            log.info("Meldung: {}", value);
        }
    }
} 
