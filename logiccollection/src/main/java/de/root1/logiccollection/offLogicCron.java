package de.root1.logiccollection;

import de.root1.kad.logicplugin.Logic;
import de.root1.kad.logicplugin.cron.Scheduled;

public class offLogicCron extends Logic {
    
    @Override
    public void init() {
        log.info("CRON test is running.");
    }
    
    @Scheduled(cron = "*/1 * * * *")
    public void ping() {
        log.info("### PONG ###");
    }

}

