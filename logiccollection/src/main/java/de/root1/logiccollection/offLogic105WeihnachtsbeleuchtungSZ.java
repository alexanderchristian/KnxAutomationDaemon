package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.logicplugin.Logic;
import java.util.StringTokenizer;

/**
 * Wacht über die Eg Szenen.
 *
 * Wenn SCHLAFEN dann Sperre der Deko-Beleuchtung im Schlafzimmer Wenn
 * ALLES_ANDERE dann Sperre aufheben
 *
 *
 * Die Deko geht an, wenn die Dämmerung einsetzt. Das ist (gemäß einstellung
 * Wetterstation VOR den Rolläden) Die Deko geht aus, wenn der Tag anbricht.
 *
 * Schaltet die Dämmerung die Deko an, wird gleich durch die Logik der Rolladen
 * gesperrt
 *
 *
 *
 *
 * @author achristian
 */
public class offLogic105WeihnachtsbeleuchtungSZ extends Logic {

    KnxAddress gaScene;
    KnxAddress gaSperreDeko;
    KnxAddress gaDekoStatus;
    KnxAddress gaRolladenSperre;
    KnxAddress gaRolladenAbs;

    @Override
    public void init() {
        setPA("1.0.105");

        gaScene = getAddress("Szenen EG Gesamt");
        gaSperreDeko = getAddress("Licht Sperren Schlafzimmer Deko");
        gaDekoStatus = getAddress("Licht Status Schlafzimmer Deko");
        gaRolladenSperre = getAddress("Sonnenschutz Sperren Schlafzimmer");
        gaRolladenAbs = getAddress("Sonnenschutz Bewegen Schlafzimmer - Abs. Position");

        listenTo(gaScene);
        listenTo(gaDekoStatus);
    }

    /**
     * Wenn Dämmerung -> Deko EIN
     *
     */
    @Override
    public void onDataWrite(KnxAddress ga, String value) {

        /**
         * Deko geht (gesteuert durch Dämmerung) an, Rolladen (der kurz danach
         * fahren würde) muss gesperrt werden
         */
        if (ga.equals(gaDekoStatus)) {
            if (getValueAsBoolean(value)) {
                log.info("Schlafzimmer-Deko geht an. Rolladen im Schlafzimmer sperren!");
                write(gaRolladenSperre, "1");
            }
        }

        /**
         * Steuerung Deko-Sperre
         */
        if (ga.equals(gaScene)) {
            StringTokenizer st = new StringTokenizer(value);
            boolean activate = st.nextToken().equalsIgnoreCase("activate");
            int sceneIndex = Integer.parseInt(st.nextToken());
            log.info("Szene: {} Status: {}", sceneIndex, activate ? "aktivieren" : "lernen");

            if (activate) {

                if (sceneIndex == 6) {
                    // Schlafen
                    log.info("Schlaf-Modus. Deko sperren.");
                    write(gaSperreDeko, "1");

                    log.info("Schlaf-Modus. Rolladen auf 100% setzen.");
                    write(gaRolladenAbs, "100");

                    log.info("Schlaf-Modus. Rolladen Sperre aufheben damit er die gewünschte Position anfährt.");
                    write(gaRolladenSperre, "0");

                } else {
                    // alles andere
                    log.info("kein Schlafmodus. Etwaige Deko sperre aufheben.");
                    write(gaSperreDeko, "0");
                }
            }
        }
    }
}
