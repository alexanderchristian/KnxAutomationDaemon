//package de.root1.logiccollection;
//
//import de.root1.kad.logic.lib.blindscontrol.BlindsControl;
//import de.root1.kad.knxservice.KnxServiceException;
//import de.root1.kad.logicplugin.Logic;
//import de.root1.kad.logicplugin.cron.Scheduled;
//import de.root1.sunposition.SunPosition;
//
//public class offLogicBlindsControl extends Logic {
//    
//    private final SunPosition sunPosition = new SunPosition(49.2976823D, 8.9664725D);
//    
//    private final BlindsControl bcLichtbandLinks = new BlindsControl(this, "LichtbandLinks", sunPosition);
//    private final BlindsControl bcLichtbandRechts= new BlindsControl(this, "LichtbandRechts", sunPosition);
//    private final BlindsControl bcFixLinks= new BlindsControl(this, "FixLinks", sunPosition);
//    private final BlindsControl bcTerrasse= new BlindsControl(this, "Terrasse", sunPosition);
//    private final BlindsControl bcFixRechts= new BlindsControl(this, "FixRechts", sunPosition);
//
//    private final BlindsControl bcKueche= new BlindsControl(this, "Küche", sunPosition);
//    
//    @Override
//    public void init() {
//        log.info("Blinds control is running.");
//        
//        bcLichtbandLinks.setGaBlindsPosition("Sonnenschutz Bewegen Wohnen Lichtband links - Abs. Position");
//        bcLichtbandLinks.setGaBlindsSlat("Sonnenschutz Bewegen Wohnen Lichtband links - Abs. Lamellenposition");
//        bcLichtbandLinks.setGaCurrentBlindsPosition("Sonnenschutz Status Lichtband links - akt. Position");
//        
//        bcLichtbandRechts.setGaBlindsPosition("Sonnenschutz Bewegen Wohnen Lichtband rechts - Abs. Position");
//        bcLichtbandRechts.setGaBlindsSlat("Sonnenschutz Bewegen Wohnen Lichtband rechts - Abs. Lamellenposition");
//        bcLichtbandRechts.setGaCurrentBlindsPosition("Sonnenschutz Status Lichtband rechts - akt. Position");
//        
//        bcLichtbandLinks.setAltitudeWindow(0, 65);
//        bcLichtbandLinks.setAzimuthWindow(125, 220);a
//        
////        bcLichtbandLinks.setAltitudeWindow(0, 65);
////        bcLichtbandLinks.setAzimuthWindow(0, 330);
//        
//        bcLichtbandLinks.setBlindsSunPosition(100);
//        bcLichtbandLinks.setBlindsVerticalPositionTolerance(5);
//        
//        // Optional for blinds with slats
//        bcLichtbandLinks.setSlatsHorizontalPosition(50);
//        bcLichtbandLinks.setSlatsVerticalPosition(100);
//        bcLichtbandLinks.setSlatsStandbyPosition(50);
//        bcLichtbandLinks.setSlatsMinimumChange(2);
//        
//        bcLichtbandLinks.setWarmOrSunny(true);
//        
//        bcLichtbandLinks.setEnabled(true);
//        bcLichtbandLinks.start();
//    }
//    
//    // every minute
//    @Scheduled(cron = "*/1 * * * *")
//    public void doControl() {
//        bcLichtbandLinks.update();
//    }
//
//    @Override
//    public void onData(String ga, String value, TYPE type) throws KnxServiceException {
//        // call super to get the external listeners (used by the BlindsControl-lib) informed about the event
//        super.onData(ga, value, type);
//    }
//    
//}
//
