package samplescripts;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;

public class Logic104InvertDayNightMode extends Logic {

    String pa = "1.0.104";
    KnxAddress ga;
    KnxAddress gaInvert;

    @Override
    public void init() {
        setPA(pa);

        ga = getAddress("Daemmerung Tag-Nacht Modus");
        gaInvert = getAddress("Daemmerung Tag-Nacht Modus invertiert");

        listenTo(ga);
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {
        boolean mode = getValueAsBoolean(value);
        write(gaInvert, getBooleanAsValue(!mode));
        log.info("Inverting day-night mode");
    }

}
