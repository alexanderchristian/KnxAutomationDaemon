package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;

public class Logic108MdtLedOffProxy extends Logic {

    KnxAddress gaKuecheProxy;
    KnxAddress gaKueche; // Output GA
    KnxAddress gaStateKueche;

    KnxAddress gaEssenProxy; // Input GA
    KnxAddress gaEssen; // Output GA
    KnxAddress gaStateEssen;

    private boolean stateEssen;
    private boolean stateKueche;

    @Override
    public void init() {
        setPA("1.0.108");

        gaKuecheProxy = getAddress("Licht Schalten Küche Proxy"); // Input GA
        gaKueche = getAddress("Licht Schalten Küche"); // Output GA
        gaStateKueche = getAddress("Licht Status Küche");

        gaEssenProxy = getAddress("Licht Schalten Essen Proxy"); // Input GA
        gaEssen = getAddress("Licht Schalten Essen"); // Output GA
        gaStateEssen = getAddress("Licht Status Essen");

        listenTo(gaKuecheProxy);
        listenTo(gaEssenProxy);
        try {
            stateEssen = getValueAsBoolean(read(gaStateEssen));
            log.info("State Essen: {}", stateEssen);
            stateKueche = getValueAsBoolean(read(gaStateKueche));
            log.info("State Küche: {}", stateKueche);
        } catch (KnxServiceException ex) {
            log.debug("Error getting on/off state", ex);
        }
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {
        boolean state = getValueAsBoolean(value);

        if (ga.equals(gaEssenProxy)) {
            if (state != stateEssen) {
                stateEssen = state;
                write(gaEssen, getBooleanAsValue(state));
                log.info("Setting '{}' to {}", gaEssen, state);
            } else {
                log.info("Skipping duplicate state on {}: {}", gaEssen, state);
            }
        } else if (ga.equals(gaKuecheProxy)) {
            if (state != stateKueche) {
                stateKueche = state;
                write(gaKueche, getBooleanAsValue(state));
                log.info("Setting '{}' to {}", gaEssen, state);
            } else {
                log.info("Skipping duplicate state on {}: {}", gaKueche, state);
            }
        }
    }
}
