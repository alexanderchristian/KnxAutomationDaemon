package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;
import java.util.StringTokenizer;

public class Logic110SceneBasedNightMode extends Logic {

    KnxAddress gaSzenenEG;
    KnxAddress gaDayNightSceneMode;

    @Override
    public void init() {
        setPA("1.0.110");

        gaSzenenEG = getAddress("Szenen EG Gesamt");
        gaDayNightSceneMode = getAddress("Daemmerung Tag-Nacht Modus Szenenbasiert");

        listenTo(gaSzenenEG);
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {
        if (ga.equals(gaSzenenEG)) {

            StringTokenizer st = new StringTokenizer(value);
            boolean activate = st.nextToken().equalsIgnoreCase("activate");
            int sceneIndex = Integer.parseInt(st.nextToken());

            if (activate) {
                if (sceneIndex == 6) {
                    log.info("Scene based night mode ON");
                    write(gaDayNightSceneMode, "1"); // nacht modus an
                } else {
                    log.info("Scene based night mode OFF");
                    write(gaDayNightSceneMode, "0"); // nacht modus aus
                }
            }

        }
    }
}
