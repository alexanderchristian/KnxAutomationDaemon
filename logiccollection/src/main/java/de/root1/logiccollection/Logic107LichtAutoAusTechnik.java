package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;

public class Logic107LichtAutoAusTechnik extends Logic {

    KnxAddress gaLichtBad;
    KnxAddress gaPräsenzBad;

    boolean lastState = false;

    @Override
    public void init() {
        setPA("1.0.107");
        
        gaLichtBad = getAddress("Licht Schalten Technik");
        gaPräsenzBad = getAddress("Präsenz Technik");
        
        listenTo(gaPräsenzBad);

        try {
            lastState = getValueAsBoolean(read(gaPräsenzBad));
        } catch (KnxServiceException ex) {
            log.warn("not able to retrieve last state", ex);
        }

        log.info("Starting with last state = {}", lastState);
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {
        boolean state = getValueAsBoolean(value); // get presence state from knx event
        log.info("Received presence state: {}", state);
        if (lastState && !state) { // if presence is gone ...
            write(gaLichtBad, getBooleanAsValue(false)); // ... turn off the light
            log.info("Light Auto-OFF");
        }
        lastState = state; // store last state
    }

}
