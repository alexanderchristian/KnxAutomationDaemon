package de.root1.logiccollection;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.logicplugin.Logic;
import java.util.StringTokenizer;

public class Logic103SceneEG extends Logic {

    String pa = "1.1.103";
    KnxAddress sceneGA;

    String prop1DimWozi = "scene.1.dim.wozi";
    String prop1StatEssZi = "scene.1.stat.esszi";

    String prop2DimEssZi = "scene.2.dim.esszi";
    String prop2StatEssZi = "secene.2.stat.esszi";

    @Override
    public void init() {
        setPA(pa);
        sceneGA = getAddress("Szenen EG Gesamt");
        listenTo(sceneGA);
    }

    @Override
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {

        if (ga.equals(sceneGA)) {
            StringTokenizer st = new StringTokenizer(value);
            boolean activate = st.nextToken().equalsIgnoreCase("activate");
            int sceneIndex = Integer.parseInt(st.nextToken());
            log.info("Szene: {} Status: {}", sceneIndex, activate ? "aktivieren" : "lernen");

            if (activate) {

                // Activate
                switch (sceneIndex) {

                    // Normal Wohnen/Essen
                    case 0:
                        log.info("Aktiviere Szene 'Normal'");
                        write(getAddress("Szene 1"), "1");
                        write(getAddress("Szene 2"), "0");
                        write(getAddress("Szene 3"), "0");
                        write(getAddress("Szene 7"), "0");

                        write(getAddress("Licht Sperren Wohnzimmer"), "0");
                        write(getAddress("Licht Sperren Esszimmer"), "0");

                        write(getAddress("Licht Schalten Schlafzimmer"), "0");

                        write(getAddress("Licht Schalten Flur EG Wandlampen"), "0");
                        write(getAddress("Licht Schalten Eingang"), "0");

                        break;

                    // Fernsehen
                    case 1:
                        log.info("Aktiviere Szene 'Fernsehen'");
                        write(getAddress("Szene 1"), "0");
                        write(getAddress("Szene 2"), "1");
                        write(getAddress("Szene 3"), "0");
                        write(getAddress("Szene 7"), "0");

                        write(getAddress("Licht Dimmen Abs. Wohnen"), config.getProperty(prop1DimWozi, "3"));
                        write(getAddress("Licht Sperren Esszimmer"), "1");

                        write(getAddress("Licht Sperren Wohnzimmer"), "0");

                        break;

                    // Essen
                    case 2:
                        log.info("Aktiviere Szene 'Essen'");
                        write(getAddress("Szene 1"), "0");
                        write(getAddress("Szene 2"), "0");
                        write(getAddress("Szene 3"), "1");
                        write(getAddress("Szene 7"), "0");

                        write(getAddress("Licht Dimmen Abs. Essen"), config.getProperty(prop2DimEssZi, "50")); // default 50%
                        write(getAddress("Licht Sperren Esszimmer"), "0");

                        write(getAddress("Licht Schalten Esstisch"), config.getProperty(prop2StatEssZi, "1"));
                        break;

                    // putzen    
                    case 5:
                        log.info("Aktiviere Szene 'Putzen'");
                        write(getAddress("Szene 1"), "0"); // index 0 Normal
                        write(getAddress("Szene 2"), "0"); // index 1 Fernsehen
                        write(getAddress("Szene 3"), "0"); // index 2 Essen
                        write(getAddress("Szene 7"), "0");  // index 6 Schlafen

                        write(getAddress("Licht Schalten Schlafzimmer"), "1");

                        // Alles an
                        write(getAddress("Licht Dimmen Abs. Essen"), "100");
                        write(getAddress("Licht Dimmen Abs. Wohnen"), "100");

                        write(getAddress("Licht Dimmen Abs. Kueche"), "100");
                        write(getAddress("Licht Dimmen Abs. Eingang"), "100");

                        write(getAddress("Licht Dimmen Abs. Flur EG Wandlampen"), "100");

                        write(getAddress("Licht Dimmen Abs. Bad EG Wandleuchten"), "100");

                        break;

                    // Schlafen EG
                    case 6:
                        log.info("Aktiviere Szene 'Schlafen EG'");
                        write(getAddress("Szene 1"), "0");
                        write(getAddress("Szene 2"), "0");
                        write(getAddress("Szene 3"), "0");
                        write(getAddress("Szene 7"), "1");

                        write(getAddress("Licht Schalten Küche Proxy"), "0");

                        write(getAddress("Licht Sperren Wohnzimmer"), "1");
                        write(getAddress("Licht Sperren Esszimmer"), "1");
                        break;
                }

            } else {

                // learn/save
                switch (sceneIndex) {

                    // Normal Wohnen/Essen
                    case 0:
                        break;

                    // Fernsehen
                    case 1:
                        config.setProperty(prop1DimWozi, read(getAddress("Licht Wert Wohnen")));
                        config.setProperty(prop1StatEssZi, read(getAddress("Licht Status Esstisch")));
                        break;

                    // Essen
                    case 2:
                        config.setProperty(prop2DimEssZi, read(getAddress("Licht Wert Essen")));
                        config.setProperty(prop2StatEssZi, read(getAddress("Licht Status Esstisch")));
                        break;

                    // Schlafen EG
                    case 3:
                        break;
                }
                saveConfig();

            }
        }

    }

}
