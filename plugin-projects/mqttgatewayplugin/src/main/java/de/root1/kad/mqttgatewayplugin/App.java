package de.root1.kad.mqttgatewayplugin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hivemq.client.mqtt.datatypes.MqttQos;
import com.hivemq.client.mqtt.mqtt5.Mqtt5AsyncClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;
import de.root1.kad.KadPlugin;
import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import de.root1.kad.knxservice.KnxServiceDataListener;
import java.util.List;
import java.util.UUID;

public class App extends KadPlugin {

    private KnxService knxService;
    private Mqtt5AsyncClient client;
    private final Gson gson = new Gson();

    private void onMqttMessage(Mqtt5Publish p) {
        try {
            log.debug("got message on topic {}: {}", p.getTopic(), p);
            String payload = new String(p.getPayloadAsBytes());
            JsonObject jsonObject = new JsonParser().parse(payload).getAsJsonObject();
            String address = jsonObject.get("address").getAsString();
            String value = jsonObject.get("value").getAsString();
            String ia = jsonObject.get("ia").getAsString();
            knxService.write(ia, KnxAddress.getAddress(address), value);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void startPlugin() {
        List<KnxService> service = getService(KnxService.class);
        if (service.size() > 1) {

        } else {
            knxService = service.get(0);
        }

        String host = configProperties.getProperty("host", "localhost");
        String user = configProperties.getProperty("user", "mqttuser");
        String pass = configProperties.getProperty("pass", "mqttpass");
        
        client = Mqtt5Client.builder()
                .identifier("kad-mqtt-gateway_" + UUID.randomUUID().toString())
                .serverHost(host)
                .simpleAuth()
                .username(user)
                .password(pass.getBytes())
                .applySimpleAuth()
                .buildAsync();

        log.info("Connecting...");
        client.connect().whenComplete((conn, throwable) -> {

            if (throwable != null) {
                
                throwable.printStackTrace();

            } else {
                log.info("Connected!");
                
                // subscribe to knx/send topic
                client.subscribeWith()
                        .topicFilter("knx/send/#")
                        .qos(MqttQos.AT_LEAST_ONCE)
                        .callback(p -> onMqttMessage(p))
                        .send();
                log.info("Subscribed!");

                // register for all KNX addresses
                knxService.registerListener(KnxAddress.WILDCARD, new KnxServiceDataListener() {

                    @Override
                    public void onData(KnxAddress addr, String value, KnxServiceDataListener.TYPE type) {
                        switch (type) {
                            case WRITE:
                                log.debug("Got 'write', forwarding to MQTT: [{}] -> \"{}\"", addr, value);
                                String topic = "knx/received/" + addr.getAddress();

                                JsonObject jo = new JsonObject();
                                jo.addProperty("value", value);
                                jo.addProperty("dpt", addr.getDPT());
                                jo.addProperty("address", addr.getAddress());
                                jo.addProperty("nameaddress", addr.getNameAddress());

                                client.publishWith()
                                        .topic(topic)
                                        .qos(MqttQos.EXACTLY_ONCE)
                                        .payload(gson.toJson(jo).getBytes())
                                        .send();

                                break;
                            default:
                                log.warn("Not supported KNX message: {}", type);
                        }
                    }
                });
                
                log.info("Listening on KNX!");
            }

        });

    }

    @Override
    public void stopPlugin() {
        if (client != null) {
            client.disconnect();
            log.info("Stopped");
        }
    }

    @Override
    public String getPluginId() {
        return getClass().getCanonicalName();
    }

}
