/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KAD Logic Plugin (KLP).
 *
 *   KLP is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KLP is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KLP.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.logicplugin;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.knxservice.KnxSimplifiedTranslation;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
public abstract class Logic implements LogicKnxEventListener {

    public final Logger log = LoggerFactory.getLogger(getClass());
    private KnxService knx;
    private final Set<KnxAddress> groupAddresses = new HashSet<>();
    private String pa;
    protected Properties config = new Properties();
    private final File confFile;
    private final String id = getClass().getCanonicalName();
    
    /**
     * list of listeners which are executed if any "listenTo" GA occurs
     */
    private final List<LogicKnxEventListener> externalListeners = new ArrayList<>();
    
    void loadConfig() {
        if (confFile.exists()) {
            try (FileInputStream fis = new FileInputStream(confFile)) {
                config.load(fis);
            } catch (IOException ex) {
                log.error("", ex);
            }
        }
    }
    
    public void saveConfig() {
        try (FileOutputStream fos = new FileOutputStream(confFile)){
            config.store(fos, "Logic "+id+", saved "+new Date().toString());
        } catch (IOException ex) {
            log.error("",ex);
        }
    }

    public enum TYPE {
        READ, WRITE, RESPONSE, UNDEFINED
    };

    public Logic() {
        confFile = new File(de.root1.kad.utils.Utils.getConfDir(), "logic_" + id + ".properties");
    }

    public void listenTo(KnxAddress ga) {
        groupAddresses.add(ga);
        log.info("{} now listens to {}", getClass().getCanonicalName(), ga);
    }
    
    /**
     * 
     * @param stringAddress
     * @return 
     */
    public KnxAddress getAddress(String stringAddress) {
        return KnxAddress.getAddress(stringAddress);
    }
    
    
    /**
     * If you pass a logic to a library, that itself wants to register for knx events, use this addListener methode to add those listeners
     * <b>Ensure that the logic itself (if used/overwritten) calls super.onData(). Otherwise listners aren't triggered!</b>
     * @param listener 
     */
    public void addListener(LogicKnxEventListener listener) {
        synchronized(externalListeners) {
            externalListeners.add(listener);
            log.info("{} now has external listener", getClass().getCanonicalName());
        }
    }

    /**
     * Called by th e logicengine when the logic gets loaded/started
     */
    public abstract void init();
    
    /**
     * Called by the logicengine to unload/stop the logic, f.i. when applying external changes/updates to the logic
     */
    public void shutdown(){
        
    }

    /**
     * Called by the logicengine when a new KNX event is received on one of the "listenTo" addresses
     * @param ga
     * @param value
     * @throws KnxServiceException 
     */
    public void onDataWrite(KnxAddress ga, String value) throws KnxServiceException {

    }

    /**
     * can be overwritten by logic script. When overwriting, don't forget to call super.onData() to get the external listeners informed about the event
     * @param ga
     * @param value
     * @param type
     * @throws KnxServiceException 
     */
    @Override
    public void onData(KnxAddress ga, String value, TYPE type) throws KnxServiceException {
        for (LogicKnxEventListener externalListener : externalListeners) {
            externalListener.onData(ga, value, type);
        }
    }

    List<KnxAddress> getGroupAddresses() {
        return new ArrayList<>(groupAddresses);
    }

    public void setPA(String pa) {
        this.pa = pa;
    }

    @Override
    public String toString() {
        return "Logic[" + getClass().getCanonicalName() + (pa != null ? "@" + pa : "") + "]";
    }

    void setKnxService(KnxService knx) {
        log.trace("Setting knxservice");
        this.knx = knx;
    }

    public void write(KnxAddress ga, String stringData) throws KnxServiceException {
        if (pa != null) {
            knx.write(pa, ga, stringData);
        } else {
            knx.write(ga, stringData);
        }
    }

    public void writeResponse(KnxAddress ga, String stringData) throws KnxServiceException {
        if (pa != null) {
            knx.writeResponse(pa, ga, stringData);
        } else {
            knx.writeResponse(ga, stringData);
        }
    }

    public String read(KnxAddress ga) throws KnxServiceException {
        if (pa != null) {
            return knx.read(pa, ga);
        } else {
            return knx.read(ga);
        }
    }

    // ----------------
    // Helper methods
    // ----------------
    public boolean getValueAsBoolean(String value) {
        switch (KnxSimplifiedTranslation.decode("1.001", value)) {
            case "on":
            case "1":
                return true;

            case "off":
            case "0":
                return false;
        }
        throw new IllegalArgumentException("Value '" + value + "' can not be converted to boolean");
    }
    
    public String getBooleanAsValue(boolean b) {
        return b ? "1" : "0";
    }
    
    public void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            log.warn("sleep was interrupted", ex);
        }
    }

    
    
}
