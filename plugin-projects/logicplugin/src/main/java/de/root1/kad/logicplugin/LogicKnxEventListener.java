/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad.logicplugin;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxServiceException;

/**
 *
 * @author achristian
 */
public interface LogicKnxEventListener {
    
    /**
     * 
     * @param ga
     * @param value
     * @param type
     * @throws KnxServiceException 
     */
    public void onData(KnxAddress ga, String value, Logic.TYPE type) throws KnxServiceException;
    
}
