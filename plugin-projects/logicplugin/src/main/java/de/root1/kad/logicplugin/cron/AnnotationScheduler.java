package de.root1.kad.logicplugin.cron;

import it.sauronsoftware.cron4j.Scheduler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cron scheduler implementation. Use AnnotationScheduler::schedule method to
 * enable scheduling for all methods annotated with Scheduled annotation.
 */
public class AnnotationScheduler {

    private Logger log = LoggerFactory.getLogger(getClass());
    private final Scheduler s;
    private final Map<Object, String> scheduleIdMap = new HashMap<>();

    public AnnotationScheduler() {
        s = new Scheduler();
        s.start();
        log.info("Scheduler started");
    }

    public void schedule(final Object obj) {
        for (Class<?> cls = obj.getClass(); cls != Object.class; cls = cls.getSuperclass()) {
            try {
                for (final Method m : cls.getDeclaredMethods()) {
                    final Scheduled annotation = m.getAnnotation(Scheduled.class);
                    if (annotation != null) {
                        m.setAccessible(true);

                        String id = s.schedule(annotation.cron(), new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    Thread.currentThread().setName("CronJob");
                                    m.invoke(obj);
                                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                                    log.warn("cannot execute cron method: " + m.toGenericString() + " at [" + annotation.cron(), ex);
                                }
                            }
                        });
                        scheduleIdMap.put(obj, id);
                        log.debug("Scheduled {}@{} with ID {}", obj.getClass().getCanonicalName(),obj.hashCode(), id);

                    }
                }
            } catch (SecurityException ex) {
                log.error("SecurityException while schduling cron method", ex);
            }
        }
    }

    public void cancel(final Object obj) {
        if (obj==null) {
            return;
        }
        if (scheduleIdMap.containsKey(obj)) {
            String id = scheduleIdMap.get(obj);
            log.debug("Deschedule {}@{} with ID {}", obj.getClass().getCanonicalName(),obj.hashCode(), id);
            s.deschedule(id);
        } else {
            log.warn("Trying to deschedule {}@{} which is not scheduled?!", obj.getClass().getCanonicalName(),obj.hashCode());
        }
    }

    public void shutdown() {
        s.stop();
        log.info("Scheduler stopped");
    }

}
