/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KAD Logic Plugin (KLP).
 *
 *   KLP is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KLP is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KLP.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.logicplugin;

import de.root1.kad.logicplugin.cron.AnnotationScheduler;
import de.root1.kad.KadPlugin;
import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import de.root1.kad.knxservice.KnxServiceConfigurationException;
import de.root1.kad.knxservice.KnxServiceDataListener;
import de.root1.kad.knxservice.KnxServiceException;
import de.root1.kad.utils.folderwatch.DefaultFolderWatchListener;
import de.root1.kad.utils.folderwatch.FolderWatch;
import de.root1.kad.utils.folderwatch.FolderWatchListener;
import de.root1.kad.utils.folderwatch.TooMuchFilesException;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author achristian
 */
public class LogicPlugin extends KadPlugin {

    private final File srcDir = new File(getBaseDir() + File.separator + "logic", "src");
    private final File libDir = new File(getBaseDir() + File.separator + "logic", "lib");

    /**
     * List with all loaded logics
     */
//    private final List<Logic> logicList = new ArrayList<>();
    /**
     *
     */
    private final List<SourceContainer> sourceContainerList = new ArrayList<>();

    /**
     * GaName -> List<Logic>
     */
    private final Map<KnxAddress, List<Logic>> gaLogicMap = new HashMap<>();

    private KnxService knx;
    private AnnotationScheduler scheduler;

    KnxServiceDataListener listener = new KnxServiceDataListener() {

        @Override
        public void onData(KnxAddress ga, String value, KnxServiceDataListener.TYPE type) {

            // forward events from KNX to relevant logic
            HashSet<Logic> list = new HashSet<>();
            if (gaLogicMap.containsKey(ga)) {
                list.addAll(gaLogicMap.get(ga));
            }
            if (gaLogicMap.containsKey(knx.getAddress("*"))) {
                list.addAll(gaLogicMap.get(knx.getAddress("*")));
            }

            Logic.TYPE logicType = Logic.TYPE.UNDEFINED;
            switch (type) {
                case READ:
                    logicType = Logic.TYPE.READ;
                    break;
                case WRITE:
                    logicType = Logic.TYPE.WRITE;
                    break;
                case RESPONSE:
                    logicType = Logic.TYPE.RESPONSE;
                    break;
                case UNDEFINED:
                    log.warn("UNDEFINED message type. will not forward.");
                    return;
            }
            for (Logic logic : list) {
                log.debug("Forwarding value '{}' from [{}] to {}", new Object[]{value, ga, logic.toString()});
                try {
                    switch (logicType) {
                        case WRITE:
                            logic.onDataWrite(ga, value);
                            break;
                    }
                    logic.onData(ga, value, logicType);
                } catch (KnxServiceException ex) {
                    ex.printStackTrace();
                }
            }
        }
    };

    private SourceContainer findSourceContainer(File f) {
        f = de.root1.kad.utils.Utils.shortenFile(f);
        for (SourceContainer sc : sourceContainerList) {
            if (sc.getFile().equals(f)) {
                // found matching file
                return sc;
            }
        }
        return null;
    }

    @Override
    public void startPlugin() {
        log.info("Starting ...");

        knx = getService(KnxService.class).get(0);
        try {
            knx.registerListener(knx.getAddress("*"), listener);
        } catch (KnxServiceConfigurationException ex) {
            log.error("Error registering wildcard listener on knx service", ex);
        }
        scheduler = new AnnotationScheduler();
        
        // initial read source files
        sourceContainerList.addAll(Utils.getSourceContainers(srcDir, libDir));
        log.info("Found logicscripts: {}", sourceContainerList);

        for (SourceContainer sc : sourceContainerList) {
            loadSourceContainer(sc);
        }

        /*
         * TODO If srcDir content changes: reload/load related source: -->
         * differ between 
         *    -> load 
         *    -> reload 
         *    -> unload
         *
         * If libDir changes: reload all (!) sources
         */
        try {
            FolderWatch fw = new FolderWatch("Logic");
            fw.addListener(srcDir, new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.isDirectory() || (f.getName().endsWith(".java") && !f.getName().startsWith(".")); // .java files starting with "." are not detected
                }
            }, new DefaultFolderWatchListener() {

                @Override
                public void created(File f) {
                    try {
                        log.info("New detected: {}", f.getAbsolutePath());
                        log.info("Creating new instance");
                        SourceContainer sc = new SourceContainer(srcDir, libDir, f);
                        sourceContainerList.add(sc);
                        loadSourceContainer(sc);
                    } catch (IOException ex) {
                        // should not happen, as 'f' is reported by FolderWatch
                        ex.printStackTrace();
                    }
                }

                @Override
                public void deleted(File f) {
                    log.info("Delete detected: {}", f.getAbsolutePath());
                    log.info("Removing instance");
                    SourceContainer sc = findSourceContainer(f);
                    if (sc != null) {
                        shutdownLogic(sc);
                    }
                }

                @Override
                public void modified(File f) {
                    try {
                        SourceContainer sc = findSourceContainer(f);
                        if (sc != null) {
                            log.info("Change detected: {}", sc);
                            shutdownLogic(sc);
                            log.info("Creating new instance");
                            sc = new SourceContainer(srcDir, libDir, f);
                            sourceContainerList.add(sc);
                            loadSourceContainer(sc);
                        } else {
                            log.error("Change in file which is not related to a SourceContainer? file={}", f.getAbsolutePath());
                        }

                    } catch (IOException ex) {
                        // should not happen, as 'f' is reported by FolderWatch
                        ex.printStackTrace();
                    }
                }

            });
            fw.addListener(libDir, new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.isFile() && f.getName().endsWith(".jar");
                }
            }, new FolderWatchListener() {

                @Override
                public void created(File f) {
                    triggerReload(f);
                }

                @Override
                public void deleted(File f) {
                    triggerReload(f);
                }

                @Override
                public void modified(File f) {
                    triggerReload(f);
                }

                private void triggerReload(File f) {
                    log.info("Change detected in libdir: {}", f);
                    List<SourceContainer> scList = new ArrayList<>();
                    scList.addAll(sourceContainerList);
                    for (SourceContainer sc : scList) {
                        shutdownLogic(sc);
                        try {
                            sc = new SourceContainer(srcDir, libDir, sc.getFile());
                            loadSourceContainer(sc);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            });
        } catch (TooMuchFilesException ex) {
            log.warn("Too much files to observe for live-update", ex);
        }

        log.info("Startup finished.");
    }

    private void loadSourceContainer(SourceContainer sc) {
        log.info("Loading logicscript: " + sc.getCanonicalClassName());
        try {
            sc.setKadClassloader(getKadClassLoader());
            sc.loadLogic();
            Logic logic = sc.getLogic();
            logic.loadConfig();
            logic.setKnxService(knx);
            scheduler.schedule(logic);
            log.info("Initialize logic ...");
            logic.init();
            addToGaLogicMap(logic);
            log.info("... done");
        } catch (LogicException ex) {
            log.error("Error loading script '{}': {}", sc.getPackagePath() + File.separator + sc.getJavaSourceFile(), ex.getMessage());
        } catch (LoadSourceException | RuntimeException ex) {
            log.error("Error loading source", ex);
        } 
    }

    private void shutdownLogic(SourceContainer sc) {
        try {
            log.info("Shutdown Logic {}", sc.getCanonicalClassName());
            final Logic logic = sc.getLogic();

            if (logic != null) {
                scheduler.cancel(logic);

                // shutdown logic
                logic.shutdown();

                // remove source container
                sourceContainerList.remove(sc);

                // remove from map map
                List<KnxAddress> groupAddresses = logic.getGroupAddresses();
                for (KnxAddress ga : groupAddresses) {
                    List<Logic> gaLogicList = gaLogicMap.get(ga);
                    gaLogicList.remove(logic);
                    if (gaLogicList.isEmpty()) {
                        gaLogicMap.remove(ga);
                    }
                }
            } else {
                log.info("Logic {} not started?! No stop possible.", sc.getCanonicalClassName());
            }

        } catch (Throwable t) {
            log.error("Problem while calling shutdown on logic", t);
        }
    }

    @Override
    public void stopPlugin() {
        log.info("Stopping Plugin {}", getClass().getCanonicalName());

        List<SourceContainer> clonedList = new ArrayList<>();
        clonedList.addAll(sourceContainerList);

        for (SourceContainer sc : clonedList) {
            shutdownLogic(sc);
        }
        clonedList.clear();

        scheduler.shutdown();

        if (knx != null) {
            try {
                knx.unregisterListener(knx.getAddress("*"), listener);
            } catch (KnxServiceConfigurationException ex) {
                ex.printStackTrace();
            }
            knx = null;
        } else {
            log.info("Not yet started?!");
        }
        log.info("Stopping Plugin {} *DONE*", getClass().getCanonicalName());
    }

    /**
     *
     * @param logic
     */
    private void addToGaLogicMap(Logic logic) {
        List<KnxAddress> interestedGAs = logic.getGroupAddresses();

        for (KnxAddress interestedGA : interestedGAs) {
            List<Logic> list = gaLogicMap.get(interestedGA);
            if (list == null) {
                list = new ArrayList<>();
                log.debug("Creating new gaLogicMap list for {}", interestedGA);
                gaLogicMap.put(interestedGA, list);
            }

            log.debug("Adding {} to list for {}", logic, interestedGA);
            list.add(logic);
        }

    }

    @Override
    public String getPluginId() {
        return getClass().getCanonicalName();
    }

}
