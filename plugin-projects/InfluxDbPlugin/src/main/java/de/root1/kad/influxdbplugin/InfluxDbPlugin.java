/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad.influxdbplugin;

import de.root1.kad.KadPlugin;
import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import de.root1.kad.knxservice.KnxServiceConfigurationException;
import de.root1.kad.knxservice.KnxServiceDataListener;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author alexander
 */
public class InfluxDbPlugin extends KadPlugin {

    private KnxService knxService;
    
    private NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
    private DecimalFormat df = (DecimalFormat)nf;
    

    public InfluxDbPlugin() {
        df.applyPattern("0.00");
    }
    
    KnxServiceDataListener listener = new KnxServiceDataListener() {

                private final NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault(Locale.Category.FORMAT));

                @Override
                public void onData(KnxAddress addr, String value, KnxServiceDataListener.TYPE type) {

                    log.info("addr={}, value={} type={}", addr, value, type);

                    try {
                        log.info("1");
                        double v;
                        log.info("2");
                        if (value.contains(",")) {
                            log.info("3");
                            Number number = numberFormat.parse(value);
                            log.info("4");
                            v = number.doubleValue();
                        } else {
                            log.info("5");
                            v = Double.parseDouble(value);
                            log.info("5.1");
                        }
                        log.info("6");
                        log.info("parsed value: {}", v);

                        // only store write telegrams
                        if (type == KnxServiceDataListener.TYPE.WRITE) {
                            //dao.insertValue(addr, v);
                            try {
                                log.info("7");
                                store(addr, v);
                            } catch (IOException ex) {
                                log.info("Error sending to influx db", ex);
                            }
                        }
                    } catch (NumberFormatException | ParseException ex) {
                        log.info("Can't convert value [" + value + "] from " + addr.toString() + " to double.", ex);
                    }
                }
            };

    @Override
    public void startPlugin() {

        List<KnxService> service = getService(KnxService.class);
        if (service.size() > 1) {
            throw new RuntimeException("There's more than one service '" + KnxService.class.getName() + "'. Unclear which to use. Stopping here.");
        } else {
            knxService = service.get(0);
        }

        // start listener
        try {
            knxService.registerListener(KnxAddress.WILDCARD, listener);
            log.info("Registered listener");
        } catch (KnxServiceConfigurationException ex) {
            log.error("error registering listener", ex);
        }

    }

    private void store(KnxAddress addr, double v) throws IOException {
        
        log.info("about to store {} {}", addr, v);        
        
        log.info("Prepare body");
        
        String db = "knx";
        String measurement = "knx";
        String tags = "ga=" + addr.getAddress() + ",na=" + addr.getNameAddress() + ",dpt=" + addr.getDPT();
        tags = tags.replaceAll(" ", "\\\\ "); // replace any " " with "\ "
        String fieldkey = "value=" + df.format(v);
        String timestamp = System.currentTimeMillis()*1000+"";

        String body = measurement + "," + tags + " " + fieldkey;//+ " " + timestamp;
        
        log.info("Storing in DB: {}", body);

        URL url = new URL("http://192.168.200.3:8086/write?db=" + db);
        log.info("Connecting ...");
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;
        log.info("Connected!");
        http.setRequestMethod("POST"); // POST/PUT is another valid option
        http.setDoOutput(true);
        log.info("Sending data");
        OutputStream outputStream = http.getOutputStream();
        outputStream.write(body.getBytes("UTF-8"));
        outputStream.close();
        log.info("Sent.");
        int responseCode = http.getResponseCode();
        String responseMessage = http.getResponseMessage();
        log.info("Response: responseCode={} responseMessage={}", responseCode, responseMessage);

    }

    KnxService getKnxService() {
        return knxService;
    }

    @Override
    public void stopPlugin() {
        knxService.unregisterListener(KnxAddress.WILDCARD, listener);
        log.info("Unregistered listener. Stopped.");
    }

    @Override
    public String getPluginId() {
        return getClass().getCanonicalName();
    }

}
