/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad.databaseplugin;

import java.awt.Color;
import org.rrd4j.graph.RrdGraphDef;
import org.rrd4j.graph.RrdGraph;
import org.rrd4j.ConsolFun;
import java.awt.image.BufferedImage;
import java.io.IOException;
import org.rrd4j.core.RrdBackendFactory;

/**
 *
 * @author D463
 */
public class GraphTest {

  public static void main(String[] args) throws IOException {
    RrdBackendFactory.setDefaultFactory("FILE");
            
    RrdGraphDef graphDef = new RrdGraphDef();
    graphDef.setTimeSpan((System.currentTimeMillis()/1000)-(3*60*60), (System.currentTimeMillis()/1000));
    graphDef.setWidth(1024);
    graphDef.setHeight(768);
    graphDef.datasource("x", "4_0_0.rrd", "GroupAddressValue", ConsolFun.AVERAGE);
    graphDef.line("x", new Color(0xFF, 0, 0), null, 2);
    graphDef.setFilename("./graphtest.png");
    RrdGraph graph = new RrdGraph(graphDef);
    BufferedImage bi = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
    graph.render(bi.getGraphics());
  }

}
