/*
 * Copyright (C) 2016 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KAD Database Plugin (KDP).
 *
 *   KDP is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KDP is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KDP.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.databaseplugin;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import de.root1.kad.knxservice.KnxServiceDataListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
class DatabaseKnxListener implements KnxServiceDataListener {

  private final Logger log = LoggerFactory.getLogger(getClass());
  private DatabaseAccess dao;
  private final KnxService knxService;
  private final NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault(Locale.Category.FORMAT));

  DatabaseKnxListener(KnxService knxService, DatabaseAccess dao) {
    this.knxService = knxService;
    this.dao = dao;
  }

  @Override
  public void onData(KnxAddress addr, String value, TYPE type) {

    log.debug("addr={}, value={} type={}", addr, value, type);

    try {
      double v;
      if (value.contains(",")) {
        Number number = numberFormat.parse(value);
        v = number.doubleValue();
      } else {
        v = Double.parseDouble(value);
      }
      log.debug("parsed value: {}", v);

      // only store write telegrams
      if (type == TYPE.WRITE) {
        dao.insertValue(addr, v);
      }
    } catch (NumberFormatException | ParseException ex) {
      log.warn("Can't convert value [" + value + "] from " + addr.toString() + " to double.", ex);
    }
  }

}
