/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad.databaseplugin;

/**
 *
 * @author achristian
 */
public interface DatabaseService {
    
    /**
     * Methode die checkt, ob es für einen plot neue Daten gibt... sonst muss jedesmal die DB direkt bemüht werden.
     * 
     * z.B. könnte man die "letzte (bzw. größte) entity_data.id" für eine entität benutzen. Ist die größer als die zuletzt bekannte gibt's was neues.
     * 
     * Da das Zeitraster minimal 1 Minute ist, würde sich so eine Grafik höchstens 1x pro Minute aktualisieren
     */    
    public int getLastDataId(String address);

    
    
}
