/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad.databaseplugin;

import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import org.rrd4j.ConsolFun;
import org.rrd4j.DsType;
import org.rrd4j.core.RrdBackendFactory;
import org.rrd4j.core.RrdDb;
import org.rrd4j.core.RrdDef;
import org.rrd4j.core.Sample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RRD4J benutzen!
 *
 * RRDs für
 * <pre>
 * Stunde =        60min --> 1min step, 60 values
 * Tag    =     1.440min --> 1min step, 1440 values
 * Woche  =    10.080min --> 7min step, 1440 values
 * Monat  =    43.200min --> 30min step, 1440 values
 * Jahr   =   525.600min --> 365min step, 1440 values
 * 2Jahre = 1.051.200min --> 730min step, 1440 values
 * </pre>
 *
 * @author achristian
 */
public class DatabaseAccess {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final DatabasePlugin plugin;
    private final File databasefolder;

    private static final String DS_NAME = "GroupAddressValue";

    private final Map<KnxAddress, RrdDb> databases = new HashMap<>();
    private final Map<KnxAddress, Long> lastDbUpdate = new HashMap<>();

    private RrdDb createRRD(KnxAddress addr) throws IOException {
        File f = getRrdFile(addr);
        RrdDef rrdDef = new RrdDef(f.getAbsolutePath());
        rrdDef.setStep(60); // every 60sek one slot
        rrdDef.setStartTime((System.currentTimeMillis() / 1000) - 5); // start now-5sec
        rrdDef.addDatasource(DS_NAME, DsType.GAUGE, 600 /* heartbeat in sec */, Double.NaN /* min not known */, Double.NaN /* max not known */);
        /* hour */ rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 1 /* number of steps to average */, 60 /* number of averages to store in this archive */);
        /* day */ rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 2 /* number of steps to average */, 720 /* number of averages to store in this archive */);
        /* week */ rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 7 /* number of steps to average */, 1440 /* number of averages to store in this archive */);
        /* month */ rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 30 /* number of steps to average */, 1440 /* number of averages to store in this archive */);
        /* year */ rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 365 /* number of steps to average */, 1440 /* number of averages to store in this archive */);
        /* 2year */ rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 730 /* number of steps to average */, 1440 /* number of averages to store in this archive */);
        log.info("RRD created: {}. Est. size: {} bytes", f.getAbsolutePath(), rrdDef.getEstimatedSize());
        return new RrdDb(rrdDef);
    }

    /**
     * Simple helper to create a rrd file-object from an KnxAddress
     *
     * @param addr
     * @return file
     */
    public final File getRrdFile(KnxAddress addr) {

        return new File(databasefolder, addr.getAddress().replaceAll("\\/", "_") + ".rrd");
    }

    DatabaseAccess(DatabasePlugin plugin) {
        this.plugin = plugin;

        this.databasefolder = new File(plugin.getBaseDir(), "database");
        if (!databasefolder.exists()) {
            databasefolder.mkdirs();
        }

        // when using default factory (NIO), there's an sync-thread per RRD. For file-backend, there's not
        // Performance should be also okay with file-backend. So for now, stick to this.
        RrdBackendFactory.setDefaultFactory("FILE");
        
        KnxService knxService = plugin.getKnxService();

    }

    public void close() {
        log.info("Closing RRDs ...");
        Collection<RrdDb> rrdDbs = databases.values();
        for (RrdDb rrdDb : rrdDbs) {
            try {
                log.info("Closing: " + rrdDb.dump());
                rrdDb.close();
            } catch (IOException ex) {
                log.error("Error closing rrd: " + rrdDb);
            }
        }
        log.info("Closing RRDs *done*");
    }

    /**
     * Insert data into DB
     *
     * @param ga the GA (numeric, not name)
     * @param value
     */
    void insertValue(KnxAddress addr, double value) {
        long unixtime = System.currentTimeMillis() / 1000;
        log.info("Writing [{}]=>[{}] to database with timestamp {}@{}", new Object[]{addr, value, unixtime, new Date(unixtime * 1000)});

        RrdDb rrdDb = getRRD(addr);

        if (rrdDb != null) {
            Long lastUpdate = lastDbUpdate.get(addr);
            if (lastUpdate!=null && lastUpdate == unixtime) {
                log.warn("Skipping DB insert, because last insert was at same second.");
            } else {
                try {
                    Sample sample = rrdDb.createSample(unixtime);
                    sample.setValue(0, value);
                    sample.update();
                    lastDbUpdate.put(addr, unixtime);
                } catch (IOException ex) {
                    log.error("Unable to update RRD for " + addr.toString(), ex);
                }
            }
        }
    }

//    /**
//     * Rounds the given timeMillis to nearest minute and return unixtime
//     *
//     * @param timeMillis
//     * @return unixtime
//     */
//    private long getRoundedUnixTime(long timeMillis) {
//
//        // use current time if no time is given
//        if (timeMillis == -1) {
//            timeMillis = System.currentTimeMillis();
//        }
//
//        Calendar now = GregorianCalendar.getInstance();
//        now.setTimeInMillis(timeMillis);
//
//        // 'add' causes changing larger fields if necessary
//        now.add(Calendar.SECOND, 30);
//        // clear everyting lower than a minute
//        now.set(Calendar.SECOND, 0);
//        now.set(Calendar.MILLISECOND, 0);
//
//        return now.getTimeInMillis() / 1000;
//    }
    private RrdDb getRRD(KnxAddress addr) {
        RrdDb rrdDb = databases.get(addr);

        if (rrdDb == null) {

            try {
                File rrdFile = getRrdFile(addr);
                if (rrdFile.exists()) {
                    log.trace("loading existing rrd: {}", rrdFile.getAbsolutePath());
                    rrdDb = new RrdDb(rrdFile.getAbsolutePath());
                } else {
                    log.trace("create new rrd: {}", rrdFile.getAbsolutePath());
                    rrdDb = createRRD(addr);
                }
                // add to cached instances
                databases.put(addr, rrdDb);

            } catch (IOException ex) {
                log.error("Error creating/loading rrd for " + addr.toString(), ex);
            }
        } else {
            log.trace("Using already known rrd db instance");
        }

        return rrdDb;
    }

}
