/*
 * Copyright (C) 2016 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KAD Database Plugin (KDP).
 *
 *   KDP is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KDP is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KDP.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.databaseplugin;

import de.root1.kad.KadPlugin;
import de.root1.kad.KadService;
import de.root1.kad.knxservice.KnxAddress;
import de.root1.kad.knxservice.KnxService;
import de.root1.kad.knxservice.KnxServiceConfigurationException;
import java.util.List;

/**
 *
 * @author achristian
 */
public class DatabasePlugin extends KadPlugin {

    private KnxService knxService;
    private DatabaseKnxListener listener;
    private DatabaseAccess dao;
    private DatabaseServiceImpl ds;

    @Override
    public void startPlugin() {
        List<KnxService> service = getService(KnxService.class);
        if (service.size() > 1) {
            throw new RuntimeException("There's more than one service '" + KnxService.class.getName() + "'. Unclear which to use. Stopping here.");
        } else {
            knxService = service.get(0);
        }

        dao = new DatabaseAccess(this);
        listener = new DatabaseKnxListener(knxService, dao);

        // start listener
        try {
            knxService.registerListener(KnxAddress.WILDCARD, listener);
            log.info("Registered listener");
        } catch (KnxServiceConfigurationException ex) {
            log.error("error registering listener", ex);
        }

        ds = new DatabaseServiceImpl(dao);
        registerService(ds);

    }

    KnxService getKnxService() {
        return knxService;
    }

    @Override
    public void stopPlugin() {
        if (dao != null) {
            dao.close();
        }
        try {
            knxService.unregisterListener(KnxAddress.WILDCARD, listener);
        } catch (KnxServiceConfigurationException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String getPluginId() {
        return getClass().getCanonicalName();
    }

}
