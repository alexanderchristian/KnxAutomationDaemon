/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KAD CometVisu Backend (KCVB).
 *
 *   KCVB is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KCVB is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KCVB.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.smartvisu;

import de.root1.kad.KadPlugin;
import de.root1.kad.knxservice.KnxService;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author achristian
 */
public class SmartVisuPlugin extends KadPlugin {

    private BackendServer backendServer;
    private KnxService knxService;

    @Override
    public void startPlugin() {
        List<KnxService> service = getService(KnxService.class);
        if (service.size()>1) {
            
        } else {
            knxService = service.get(0);
        }

        int port = Integer.parseInt(configProperties.getProperty("port", "8080"));
        int sessionTimeout = Integer.parseInt(configProperties.getProperty("sessiontimeout", "120000"));
        String pa = configProperties.getProperty("pa", "0.0.0");
        String documentRoot = configProperties.getProperty("documentroot", "/kad/");
        backendServer = new BackendServer(port, documentRoot, knxService, sessionTimeout);
        
        try {
            if (backendServer != null) {
                backendServer.start();
                log.info("Started smartVISU backend server");
            } else {
                log.error("Cannot start smartVISU backend server due to initialization failure.");
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error starting server", ex);
        }
    }

    @Override
    public void stopPlugin() {
        if (backendServer != null) {
            backendServer.stop();
            log.info("Stopped smartVISU backend server");
        }
    }

    @Override
    public String getPluginId() {
        return getClass().getCanonicalName();
    }
    
    

}
