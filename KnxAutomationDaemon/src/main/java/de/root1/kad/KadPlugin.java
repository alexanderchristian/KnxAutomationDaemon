/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad;

import de.root1.kad.utils.Utils;
import de.root1.spf.PluginInterface;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
public abstract class KadPlugin implements PluginInterface, KadConfiguration {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private KadMain kadmain;

    public KadPlugin() {
        // keep empty
    }
    
    /**
     * Returns KAD base directory
     * @return base dir of kad installation
     */
    public File getBaseDir() {
        return new File(System.getProperty("kad.basedir"));
    }
    
    /**
     * Creates file object for accessing plugin config xml
     * @return file object pinting to plugin config xml. File might not exist!
     */
    public File getConfigXml() {
        String id = getPluginId();
        return new File(Utils.getConfDir(), "plugin_"+id + ".xml");
    }

    public void readConfig() {
        String id = getPluginId();
        File configFile = new File(Utils.getConfDir(), "plugin_"+id + ".properties");
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(configFile);
            configProperties.load(fis);
            fis.close();
            log.info("Successfully read config from: {}", configFile.getAbsolutePath());
        } catch (FileNotFoundException ex) {
            log.debug("No configfile: {}", configFile.getAbsolutePath());
        } catch (IOException ex) {
            log.error("Not able to read config file {}", configFile.getAbsolutePath());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                // nothing to do
            }
        }
    }

    void init(KadMain kadMain) {
        this.kadmain = kadMain;
        readConfig();
    }
    
    protected void registerService(KadService service) {
        if (kadmain==null) {
            throw new IllegalStateException("Don't call registerService from plugin-constructor.");
        }
        kadmain.registerService(service);
    }
    
    public <T> List<T> getService(Class<T> serviceClass) {
        if (kadmain==null) {
            throw new IllegalStateException("Plugin not initialized with reference to KadMain");
        }
        return kadmain.getService(serviceClass);
    }
    
    public ClassLoader getKadClassLoader() {
        return kadmain.getClass().getClassLoader();
    }
    
    public Properties getKadConfig() {
        return kadmain.getConfigProperties();
    }
    
    

}
