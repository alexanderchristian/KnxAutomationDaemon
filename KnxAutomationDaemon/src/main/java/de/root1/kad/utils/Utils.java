/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KnxAutomationDaemon (KAD).
 *
 *   KAD is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KAD is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KAD.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author achristian
 */
public class Utils {
    
    private static Pattern gaPattern = Pattern.compile("^\\d{1,3}\\/\\d{1,3}\\/\\d{1,3}$");
    private static Pattern iaPattern = Pattern.compile("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$");
    
    public static File getBaseDir() {
        return new File(System.getProperty("kad.basedir", ".")).getAbsoluteFile();
    }
    
    public static File getConfDir() {
        return new File(getBaseDir(), "conf").getAbsoluteFile();
    }
    
    public static File getCacheDir() {
        return new File(getBaseDir(), "cache").getAbsoluteFile();
    }
    
    public static String shortenPath(String path) {
        return path.replace(File.separator + "." + File.separator, File.separator);
    }

    public static File shortenFile(File file) {
        try {
            String absolutePath = file.getCanonicalPath();
            String newPath = absolutePath.replace(File.separator + "." + File.separator, File.separator);
            if (absolutePath.equals(newPath)) {
                return file.getCanonicalFile();
            } else {
                return new File(newPath);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return file;
        }
    }
    
	public static String byteArrayToHex(byte[] bytearray, boolean whitespace) {
        StringBuilder sb = new StringBuilder(bytearray.length * 2);

        for (int i = 0; i < bytearray.length; i++) {
            sb.append(String.format("%02x", bytearray[i] & 0xff));
            if (i < bytearray.length - 1 && whitespace) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public static String createSHA1(File f) throws FileNotFoundException, NoSuchAlgorithmException, IOException {
        InputStream fis = new FileInputStream(f);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("SHA1");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return byteArrayToHex(complete.digest(), false);
    }
    
    public static boolean isGa(String ga) {
        Matcher matcher = gaPattern.matcher(ga);
        boolean patternOk = matcher.matches();
        if (patternOk) {
            // check range
            String[] split = ga.split("\\/");
            int main = Integer.parseInt(split[0]);
            int middle = Integer.parseInt(split[1]);
            int sub = Integer.parseInt(split[2]);

            if ((main & ~0x1F) != 0 || (middle & ~0x7) != 0 || (sub & ~0xFF) != 0) {
                // not in range
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public static boolean isIa(String ia) {
        Matcher matcher = iaPattern.matcher(ia);
        boolean patternOk = matcher.matches();
        if (patternOk) {
            // check range
            String[] split = ia.split("\\.");
            int area = Integer.parseInt(split[0]);
            int line = Integer.parseInt(split[1]);
            int member = Integer.parseInt(split[2]);

            if ((area & ~0xF) != 0 || (line & ~0xF) != 0 || (member & ~0xFF) != 0) {
                // not in range
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public static boolean isName(String addr) {
        boolean isGa = isGa(addr);
        boolean isIa = isIa(addr);
        // no GA, no IA, no Wildcard
        return (!isGa && !isIa && !addr.equals("*"));
    }
    
}
