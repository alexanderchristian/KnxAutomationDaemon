/*
 * Copyright (C) 2016 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KnxAutomationDaemon (KAD).
 *
 *   KAD is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KAD is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KAD.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.utils.folderwatch;

import java.io.File;

/**
 *
 * @author achristian
 */
public interface FolderWatchListener {

    /**
     * File has been created.<br/>
     * <b>Note: After file creation, it's common that a {@link #modified(java.io.File)} happens as well.</b>
     * @param f created file.
     */
    public void created(File f);

    /**
     * File has been deleted
     * @param f deleted file
     */
    public void deleted(File f);

    /**
     * File has been modified
     * @param f modified file
     */
    public void modified(File f);
    
}
