/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.root1.kad.utils.folderwatch;

/**
 *
 * @author achristian
 */
public class TooMuchFilesException extends Exception {

    public TooMuchFilesException() {
    }

    public TooMuchFilesException(String message) {
        super(message);
    }

    public TooMuchFilesException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooMuchFilesException(Throwable cause) {
        super(cause);
    }
    
    
    
}
