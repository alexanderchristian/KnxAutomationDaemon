/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KnxAutomationDaemon (KAD).
 *
 *   KAD is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KAD is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KAD.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.knxservice;

/**
 *
 * @author achristian
 */
public interface KnxService {

    
    /**
     * Translates an address string groupaddress to the configured groupaddress.
     * @param addrString addressname or GA 
     * @throws KnxServiceConfigurationException if name can not be resoled to address
     * @return knx address object
     */
    public KnxAddress getAddress(String addrString) throws KnxServiceConfigurationException;
    
    /**
     * Write data to knx implementation has to lookup the required GA + DPT for
     * given groupaddress name.Data is then feed into DPT conversion and written to knx bus
     *
     * @param addr
     * @param stringData data in human readable format, matching the human readable representation of the underlying DPT
     * @throws de.root1.kad.knxservice.KnxServiceException
     */
    public void write(KnxAddress addr, String stringData) throws KnxServiceException;
    public void writeResponse(KnxAddress addr, String stringData) throws KnxServiceException;

    /**
     * Same as {@link KnxService#write(java.lang.String, java.lang.String) }
     * but with an individual address as sender address instead of a default one
     *
     * @param individualAddress individual address
     * @param addr groupaddress
     * @param stringData data in human readable format, matching the human readable representation of the underlying DPT
     * @throws de.root1.kad.knxservice.KnxServiceException
     */
    public void write(String individualAddress, KnxAddress addr, String stringData) throws KnxServiceException;
    public void writeResponse(String individualAddress, KnxAddress addr, String stringData) throws KnxServiceException;

    /**
     * Read data from KNX implementation has to lookup the required GA + DPT for given groupaddress name.
     * Read request is then triggered.
     * 
     * @param addr groupaddress 
     * @return data in human readable format, matching the human readable representation of the underlying DPT
     * @throws de.root1.kad.knxservice.KnxServiceException
     */
    public String read(KnxAddress addr) throws KnxServiceException;
    
    /**
     * Same as {@link KnxService#read(java.lang.String) } but with an individual address as sender of request instead of default one
     * @param individualAddress individual address
     * @param addr groupaddress
     * @return data in human readable format, matching the human readable representation of the underlying DPT
     * @throws de.root1.kad.knxservice.KnxServiceException
     */
    public String read(String individualAddress, KnxAddress addr) throws KnxServiceException;
    
    /**
     * Returns cached value, or null if not in cache
     * @param addr
     * @return data in human readable format, matching the human readable representation of the underlying DPT, of null if not in cache
     * @throws de.root1.kad.knxservice.KnxServiceException
     */
    public String getCachedValue(KnxAddress addr) throws KnxServiceException;
    
    public void registerListener(KnxAddress addr, KnxServiceDataListener listener) throws KnxServiceConfigurationException;
    public void unregisterListener(KnxAddress addr, KnxServiceDataListener listener) throws KnxServiceConfigurationException;

}
