/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KnxAutomationDaemon (KAD).
 *
 *   KAD is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KAD is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KAD.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.knxservice;

import de.root1.kad.KadService;
import de.root1.kad.utils.folderwatch.DefaultFolderWatchListener;
import de.root1.kad.utils.folderwatch.FolderWatch;
import de.root1.kad.utils.folderwatch.TooMuchFilesException;
import de.root1.knxprojparser.GroupAddress;
import de.root1.knxprojparser.KnxProjParser;
import de.root1.knxprojparser.Project;
import de.root1.slicknx.GroupAddressEvent;
import de.root1.slicknx.GroupAddressListener;
import de.root1.slicknx.Knx;
import de.root1.slicknx.KnxException;
import de.root1.slicknx.KnxFormatException;
import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
public class KnxServiceImpl extends KadService implements KnxService {

    private Logger log = LoggerFactory.getLogger(getClass());

    private static final String DEFAULT_IA = "1.0.254";
    private static final int TX_DELAY = 150; // ms
    private long lastWrite = System.currentTimeMillis();
    private Knx knx;

    private final File confFolder = new File(System.getProperty("kad.basedir"), "conf");

    /**
     * User config, that enhances central storage with data unknown to ets
     * project
     */
    private File knxProject = new File(confFolder, "knxproject.xml");

    /**
     * GA Name -> ListenerList
     */
    private final Map<KnxAddress, Set<KnxServiceDataListener>> listeners = new HashMap<>();

//    private ExecutorService rxPool = Executors.newCachedThreadPool(new NamedThreadFactory("KnxServiceOndataForwarding"));
    private ExecutorService rxPool = Executors.newFixedThreadPool(1, new NamedThreadFactory("KnxServiceOndataForwarding"));

    private GroupAddressListener gal;

    private final KnxCache cache;

    private final Object DATA_LOCK = new Object();

    public KnxServiceImpl() {
        super();
        cache = new KnxCache(configProperties);
        this.gal = new GroupAddressListener() {

            private void processEvent(GroupAddressEvent event) {
                KnxServiceDataListener.TYPE type = KnxServiceDataListener.TYPE.WRITE;

                KnxAddress ga = null;
                String gaString = event.getDestination();
                try {
                    ga = KnxAddress.getAddress(gaString);

                    String[] split = ga.getDPT().split("\\.");
                    int mainType = Integer.parseInt(split[0]);
                    if (mainType == 0) {
                        log.warn("DPT for GA [{}] is invalid. Check your configuration!", ga);
                        return;
                    }
                    log.debug("Decoding: ga: {}", ga);
                    String value = event.asString(mainType, ga.getDPT()); // slicknx/calimero string style
                    switch (event.getType()) {
                        case GROUP_READ:
                            type = KnxServiceDataListener.TYPE.READ;
                            value = null;
                            break;
                        case GROUP_RESPONSE:
                            type = KnxServiceDataListener.TYPE.RESPONSE;
                            value = KnxSimplifiedTranslation.decode(ga.getDPT(), value);
                            break;
                        case GROUP_WRITE:
                            type = KnxServiceDataListener.TYPE.WRITE;
                            break;
                    }

                    fireKnxEvent(ga, value, type);

                } catch (KnxFormatException ex) {
                    log.warn("Error converting data to String with DPT" + ga.getDPT() + ". event=" + event + " ga=" + ga, ex);
                } catch (KnxServiceConfigurationException ex) {
                    if (log.isTraceEnabled()) {
                        log.warn("Cannot process event for " + gaString, ex);
                    } else {
                        log.warn("Cannot process event for {}: {}", gaString, ex.getMessage());
                    }
                }
            }

            @Override
            public void readRequest(GroupAddressEvent event) {
                processEvent(event);
            }

            @Override
            public void readResponse(GroupAddressEvent event) {
                processEvent(event);
            }

            @Override
            public void write(final GroupAddressEvent event) {
                processEvent(event);
            }

        };

        log.info("Reading knx project data ...");
        readKnxProjectData();

        try {
            FolderWatch fw = new FolderWatch("Conf");
            fw.addListener(confFolder, new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.getName().endsWith(".knxproj") || f.getName().equals(knxProject.getName());
                }
            }, new DefaultFolderWatchListener() {

                @Override
                public void modified(File f) {
                    log.info("Change in {} detected. Reload scheduled.", f.getName());
                    readKnxProjectData();
                }

            });
        } catch (TooMuchFilesException ex) {
            log.error("Error observing conf file", ex);
        }

        try {
            // TODO: configure KNX according to properties!
            knx = new Knx();
            knx.setGlobalGroupAddressListener(gal);
            knx.setIndividualAddress(configProperties.getProperty("knx.individualaddress", DEFAULT_IA));
        } catch (KnxException ex) {
            log.error("Error setting up knx access", ex);
        }

    }

    private void fireKnxEvent(final KnxAddress ga, String value, final KnxServiceDataListener.TYPE type) {

        if (ga == null) {
            log.warn("Cannot process event on {} due to null GA.");
            return;
        }

        final String finalValue = KnxSimplifiedTranslation.decode(ga.getDPT(), value); // convert to KAD string style (no units etc...)
        switch (type) {
            case RESPONSE:
            case WRITE:
                cache.update(ga, finalValue);
                break;
        }
        synchronized (listeners) {

            // get listeners for this specific ga name
            Set<KnxServiceDataListener> list = listeners.get(ga);
            if (list == null) {
                log.debug("There's no special listener for {}", ga);
                list = new HashSet<>();
            } else {
                log.debug("{} listeners for {}", list.size(), ga);
            }

            // get also wildcard listeners, listening for all addresses
            final Set<KnxServiceDataListener> globalList = listeners.get(KnxAddress.getAddress("*"));
            if (globalList != null) {
                log.debug("{} wildcard listeners", globalList.size());
            }

            for (final KnxServiceDataListener listener : list) {
                // execute listener async in thread-pool
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        log.info("Forwarding: Value '{}' to {}->{}", new Object[]{finalValue, ga, listener});
                        listener.onData(ga, finalValue, type);
                    }

                };
                rxPool.execute(r);
            }

            if (globalList != null) {
                for (final KnxServiceDataListener listener : globalList) {
                    // execute listener async in thread-pool
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            log.debug("Forwarding wildcard: Value '{}' to {}->{}", new Object[]{finalValue, ga, listener});
                            listener.onData(ga, finalValue, type);
                        }

                    };
                    rxPool.execute(r);
                }
            }

        }
    }

    private void readKnxProjectData() {

        synchronized (DATA_LOCK) {

            Map<String, KnxAddress> stringToKnxAddress = new HashMap<>();

            /**
             * TODO Install file observer for .knxproj file --> reparse on
             * change Install file observer for knxproject.xml file --> reparse
             * on change
             */
            boolean ok = false;

            try {

                File[] knxprojFiles = confFolder.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.getName().endsWith(".knxproj");
                    }
                });

                Project project = null;
                if (knxprojFiles != null && knxprojFiles.length == 1) {
                    log.info("Reading {} ... ", knxprojFiles[0].getName());
                    KnxProjParser parser = new KnxProjParser();
                    boolean exportXml = parser.exportXml(knxprojFiles[0], knxProject);
                    if (!exportXml) {
                        log.info("No change in knxproj detected. Using last parser result.");
                    }
                    project = parser.getProject();
                } else {

                    String msg = "";
                    if (knxprojFiles != null) {
                        if (knxprojFiles.length == 0) {
                            msg = "No .knxproj file detected in conf folder.";
                        } else if (knxprojFiles.length > 1) {
                            msg = "More than one .knxproj file detected in conf folder. Skip reading knxproj file.";
                        }
                    } else {
                        msg = "No .knxproj file detected in conf folder.";
                    }
                    log.warn(msg);
                    KnxProjParser parser = new KnxProjParser();
                    parser.readXml(knxProject);
                    project = parser.getProject();
                }

                if (project != null) {
                    log.info("Using KNX project: name=[{}] lastModified=[{}]", project.getName(), project.getLastModified());

                    List<GroupAddress> groupaddressList = project.getGroupaddressList();
                    log.info("Found GAs: {}", groupaddressList.size());

                    for (GroupAddress ga : groupaddressList) {
                        KnxAddress addr = new KnxAddress(ga.getName(), ga.getAddress());
                        if (KnxProjParser.hasDPT(ga)) {
                            addr.setDpt(ga.getDPT());
                            stringToKnxAddress.put(ga.getName(), addr);
                            stringToKnxAddress.put(ga.getAddress(), addr);
                        } else {
                            log.error("GA {}@{} has no configured DPT!", addr.getAddress(), addr.getNameAddress());
                        }

                    }
                    KnxAddress.setAddresses(stringToKnxAddress);
                    ok = true;
                }

            } catch (Exception ex) {
                log.warn("Error while reading file data", ex);
            } finally {
                if (!ok) {
                    log.warn("address cache not available");
                    stringToKnxAddress.clear();
                }
            }
        }
    }

    private void delay() {
        long delta = System.currentTimeMillis() - lastWrite;
        if (delta < TX_DELAY) {
            try {
                long sleeptime = TX_DELAY-delta;
                log.info("delaying write by {}ms", sleeptime);
                Thread.sleep(sleeptime);
                lastWrite = System.currentTimeMillis();
            } catch (InterruptedException ex) {
                Thread.interrupted();
            }
        }
    }

    @Override
    public void writeResponse(KnxAddress ga, String value) throws KnxServiceException {
        try {
            knx.write(true, ga.getAddress(), ga.getDPT(), value);

            fireKnxEvent(ga, value, KnxServiceDataListener.TYPE.RESPONSE);
        } catch (KnxException ex) {
            throw new KnxServiceException("Problem writing '" + value + "' with DPT " + ga.getDPT() + " to " + ga, ex);
        }
    }

    @Override
    public void writeResponse(String individualAddress, KnxAddress ga, String value) throws KnxServiceException {
        try {
            knx.setIndividualAddress(individualAddress);
            writeResponse(ga, value);
            delay();
            knx.setIndividualAddress(DEFAULT_IA);
        } catch (KnxServiceException | KnxException ex) {
            throw new KnxServiceException("Problem writing", ex);
        }
    }

    @Override
    public void write(KnxAddress ga, String value) throws KnxServiceException {

        String dpt = ga.getDPT();

        try {
            knx.write(false, ga.getAddress(), dpt, KnxSimplifiedTranslation.encode(dpt, value));
            delay();
            fireKnxEvent(ga, value, KnxServiceDataListener.TYPE.WRITE);
        } catch (KnxException ex) {
            throw new KnxServiceException("Problem writing '" + value + "' with DPT " + dpt + " to [" + ga + "]", ex);
        }
    }

    @Override
    public void write(String individualAddress, KnxAddress ga, String value) throws KnxServiceException {
        try {
            knx.setIndividualAddress(individualAddress);
            write(ga, value);
            delay();
            knx.setIndividualAddress(DEFAULT_IA);
        } catch (KnxServiceException | KnxException ex) {
            throw new KnxServiceException("Problem writing", ex);
        }
    }

    @Override
    public String read(KnxAddress ga) throws KnxServiceException {

        String dpt = ga.getDPT();

        String value = cache.get(ga);
        if (value == null) {
            try {
                value = knx.read(ga.getAddress(), dpt);
                log.info("Cache does not contain value for [{}], reading from bus.", ga);
                value = KnxSimplifiedTranslation.decode(dpt, value);
                cache.update(ga, value);
                return value;
            } catch (KnxException ex) {
                throw new KnxServiceException("Problem reading with DPT " + dpt + " from " + ga, ex);
            }
        } else {
            return value;
        }
    }

    @Override
    public String read(String individualAddress, KnxAddress ga) throws KnxServiceException {
        try {
            knx.setIndividualAddress(individualAddress);
            String read = read(ga);
            knx.setIndividualAddress(DEFAULT_IA);
            return read;
        } catch (KnxServiceException | KnxException ex) {
            throw new KnxServiceException("Problem writing", ex);
        }
    }

    @Override
    public void registerListener(KnxAddress address, KnxServiceDataListener listener) throws KnxServiceConfigurationException {
        if (address == null) {
            throw new IllegalArgumentException("address must not be null");
        }
        if (listener == null) {
            throw new IllegalArgumentException("lister must not be null");
        }
        synchronized (listeners) {
            Set<KnxServiceDataListener> list = listeners.get(address);
            if (list == null) {
                list = new HashSet<>();
                listeners.put(address, list);
            }
            boolean isNew = list.add(listener);
            if (!isNew) {
                log.warn("Tried to add the listener {} multiple times to {}", listener, address);
            }
        }
    }

    @Override
    public void unregisterListener(KnxAddress address, KnxServiceDataListener listener) throws KnxServiceConfigurationException {
        if (address == null) {
            throw new IllegalArgumentException("address must not be null");
        }
        if (listener == null) {
            throw new IllegalArgumentException("lister must not be null");
        }
        synchronized (listeners) {
            Set<KnxServiceDataListener> list = listeners.get(address);
            if (list != null) {
                list.remove(listener);
                if (list.isEmpty()) {
                    listeners.remove(address);
                }
            }
        }
    }

    @Override
    protected Class getServiceClass() {
        return KnxService.class;
    }

    @Override
    public String getCachedValue(KnxAddress ga) throws KnxServiceException {
        String value = cache.get(ga);
        return value;
    }

    public KnxAddress getAddress(String addressString) {
        return KnxAddress.getAddress(addressString);
    }

}
