/*
 * Copyright (C) 2017 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KnxAutomationDaemon (KAD).
 *
 *   KAD is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KAD is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KAD.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad.knxservice;

import de.root1.kad.utils.Utils;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author achristian
 */
public class KnxAddress implements Serializable {

    public static final KnxAddress WILDCARD = new KnxAddress("wildcard", "*");

    private String name;
    private String address;
    private String dpt;

    private static Map<String, KnxAddress> addresses = new HashMap<>();
    private static final Object LOCK = new Object();

    protected KnxAddress(String name, String address) {
        this.name = name;
        this.address = address;
    }

    protected KnxAddress(String address) {
        this.address = address;
    }

    public String getNameAddress() {
        return name;
    }

    public void setNameAddress(String nameAddress) {
        this.name = nameAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        if (Utils.isGa(address)) {
            sb.append("GA(");
        } else if (Utils.isIa(address)) {
            sb.append("IA(");
        }

        sb.append(address);
        if (!(name == null && name.isEmpty())) {
            sb.append("@");
            sb.append(name);
        }
        sb.append(")");
        if (dpt != null && !dpt.isEmpty()) {
            sb.append("[");
            sb.append(dpt);
            sb.append("]");
        } else {
            sb.append("[DPT n/a]");
        }

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.address);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KnxAddress other = (KnxAddress) obj;
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        return true;
    }

    public void setDpt(String dpt) {
        this.dpt = dpt;
    }

    public String getDPT() {
        return dpt;
    }

    public static KnxAddress getAddress(String addrString) throws KnxServiceConfigurationException {
        synchronized (LOCK) {

            // Wildcard is translated to wildcard
            if (addrString.equals("*")) {
                return new KnxAddress(addrString);
            }

            if (KnxAddress.addresses.containsKey(addrString)) {
                return KnxAddress.addresses.get(addrString);
            }

            throw new KnxServiceConfigurationException("Address [" + addrString + "] is not known. Please update configuration.");
        }
    }

    static void setAddresses(Map<String, KnxAddress> addresses) {
        synchronized (LOCK) {
            KnxAddress.addresses = addresses;
        }
    }
}
