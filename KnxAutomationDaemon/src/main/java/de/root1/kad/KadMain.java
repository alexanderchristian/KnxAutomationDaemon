/*
 * Copyright (C) 2015 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KnxAutomationDaemon (KAD).
 *
 *   KAD is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KAD is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KAD.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.root1.kad;

import de.root1.kad.utils.Utils;
import de.root1.kad.knxservice.KnxServiceImpl;
import de.root1.spf.DeploymentListener;
import de.root1.spf.PluginContainer;
import de.root1.spf.PluginInterface;
import de.root1.spf.SimplePluginFramework;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
public class KadMain {

    private static Logger log;
    private final SimplePluginFramework spf;
    private final Properties applicationProperties = new Properties();
    private final Properties configProperties = new Properties();

    static {
        log = LoggerFactory.getLogger(KadMain.class);
        String basedir = System.getProperty("kad.basedir");
        String pluginsDir = System.getProperty("kad.pluginsDir");
        String deployDelay = System.getProperty("kad.deployDelay");

        if (basedir == null) {
            System.setProperty("kad.basedir", ".");
            basedir = ".";
        }

        if (deployDelay == null) {
            System.setProperty("kad.deployDelay", "2000");
        }

        if (pluginsDir == null) {
            System.setProperty("kad.pluginsDir", basedir + File.separator + "plugins");
        }

        System.setProperty("kad.basedir", Utils.shortenFile(new File(basedir)).getAbsolutePath());

        log.info("basedir:     {}", Utils.shortenFile(new File(basedir)));
        log.info("pluginsDir:  {}", Utils.shortenFile(new File(System.getProperty("kad.pluginsDir"))));
        log.info("deployDelay: {}", System.getProperty("kad.deployDelay"));

    }

    public KadMain() throws IOException {

        try {
            applicationProperties.load(getClass().getResourceAsStream("/properties/knxautomationdaemon.properties"));
        } catch (Throwable t) {
            log.error("Cannot read application properties!");
        }

        String versionMsg = "KNX automation Daemon - Version " + applicationProperties.getProperty("application.version", "n/a") + " Build " + applicationProperties.getProperty("application.build", "n/a") + "@" + applicationProperties.getProperty("application.buildhostname", "n/a");
        log.info(versionMsg);

        File confFolder = new File(System.getProperty("kad.basedir"), "conf/");
        confFolder.mkdirs();

        // read in kad-config properties file
        File configFile = new File(confFolder, "kadconfig.properties");
        if (configFile.exists()) {
            FileInputStream fis = new FileInputStream(configFile);
            configProperties.load(fis);
            fis.close();
        }

        spf = new SimplePluginFramework(new File(System.getProperty("kad.pluginsDir")), Integer.parseInt(System.getProperty("kad.deployDelay")));

        spf.setDeploymentListener(new DeploymentListener() {

            @Override
            public void preStop(PluginInterface plugin) {
            }

            @Override
            public void postStop(PluginInterface plugin) {
            }

            @Override
            public void preStart(PluginInterface plugin) {
            }

            @Override
            public void postStart(PluginInterface plugin) {
            }

            @Override
            public void loaded(PluginInterface plugin) {
                if (plugin instanceof KadPlugin) {
                    KadPlugin kadPlugin = (KadPlugin) plugin;
                    kadPlugin.init(KadMain.this);
                    log.info("Plugin initialized: {}", kadPlugin.getClass().getCanonicalName());
                    kadPlugin.startPlugin();
                    log.info("Plugin started: {}", kadPlugin.getClass().getCanonicalName());
                } else {
                    log.error("plugin not supported? -> {}", plugin);
                }
            }

        });
        log.info("Registering main services ...");
        try {
            registerService(new KnxServiceImpl());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        log.info("Loading Plugins ...");
        spf.startLoading(true);

        // insert reference to kadmain in each plugin
        // --> required f.i. to get services
        List<PluginContainer> plugins = spf.getPluginContainerList();
        if (plugins.isEmpty()) {
            log.warn("No plugins available?!");
        }

        log.info("Running!");

        // do nothing while plugins are running... --> need to be replaced with a kind of interactive console or so...!
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new KadMain();
    }

    void registerService(KadService service) {
        synchronized (services) {
            Class serviceClass = service.getServiceClass();
            List<KadService> instances = services.get(serviceClass);
            if (instances == null) {
                instances = new ArrayList<>();
                services.put(serviceClass, instances);
            }
            instances.add(service);
        }
    }

    void unregisterService(KadService service) {

        synchronized (services) {
            Class serviceClass = service.getServiceClass();
            List<KadService> instances = services.get(serviceClass);
            if (instances != null) {
                instances.remove(service);
                if (instances.isEmpty()) {
                    services.remove(serviceClass);
                }
            }
        }

    }

    public <T> List<T> getService(Class<T> serviceClass) {
        synchronized (services) {
            return (List<T>) new ArrayList<>(services.get(serviceClass));
        }

    }

    ClassLoader getClassLoader() {
        return getClass().getClassLoader();
    }

    private final Map<Class, List<KadService>> services = new HashMap<Class, List<KadService>>();

    public Properties getConfigProperties() {
        return configProperties;
    }
    
}
